import Vue from 'vue'
import Vuex from 'vuex'
import { remote } from 'electron'
import fs from 'fs'

Vue.use(Vuex)

const state = {
  fileName: null,
  list: [],
  isListDirty: false
}

const mutations = {
  newBundle (state) {
    state.fileName = null
    state.list = []
    state.isListDirty = false
  },

  openBundle (state) {
    remote.dialog.showOpenDialog(
      {filters: [{name: 'IR Bundle', extensions: ['irbnd']}]},
      fileNames => {
        if (fileNames === undefined) {
          console.log('No file selected')
          return
        } else {
          fs.readFile(fileNames[0], 'utf-8', (err, data) => {
            if (err) {
              alert(`An error ocurred reading the file : ${err.message}`)
              return
            }
            state.fileName = fileNames[0]
            state.list = JSON.parse(data)
            state.isListDirty = false
          })
        }
      }
    )
  },

  saveBundle (state) {
    fs.writeFile(state.fileName, JSON.stringify(state.list), err => {
      if (err) {
        alert('An error ocurred creating the file ' + err.message)
        return
      }
      state.isListDirty = false
    })
  },

  saveBundleAs (state) {
    remote.dialog.showSaveDialog(
      {filters: [{name: 'IR Bundle', extensions: ['irbnd']}]},
      fileName => {
        if (fileName === undefined) {
          console.log('You didn\'t save the file')
          return
        }

        fs.writeFile(fileName, JSON.stringify(state.list), function (err) {
          if (err) {
            alert('An error ocurred creating the file ' + err.message)
            return
          }
          state.fileName = fileName
          state.isListDirty = false
        })
      }
    )
  },

  exportBundle (state) {
    function lpad (value, padding) {
      var zeroes = new Array(padding + 1).join('0')
      return (zeroes + value).slice(-padding)
    }

    remote.dialog.showOpenDialog(
      {
        filters: [{name: 'IR Bundle', extensions: ['irbnd']}],
        properties: ['openDirectory', 'createDirectory'],
        buttonLabel: 'Export'
      },
      filePaths => {
        if (filePaths === undefined) {
          console.log('No file selected')
          return
        } else {
          let errorList = false
          state.list.forEach((item, index) => {
            if (fs.existsSync(item.fileName)) {
              var savedFileName = `${filePaths[0]}/${lpad(index + 1, 3)}_${item.name}.wav`
              if (!fs.existsSync(savedFileName)) {
                fs.createReadStream(item.fileName).pipe(fs.createWriteStream(savedFileName))
              }
            } else {
              state.list.splice(index, 1)
              errorList = errorList + '\n' + item.fileName
            }
          })
          if (errorList) {
            state.isListDirty = true
            remote.dialog.showErrorBox('Source files does not exist', `Cannot export files:\n${errorList}\n\nThese files have been removed from your list.`)
          }
        }
      }
    )
  },

  addIR (state) {
    remote.dialog.showOpenDialog(
      {
        filters: [{name: 'IRs', extensions: ['wav']}],
        properties: ['openFile', 'multiSelections'],
        buttonLabel: 'Add'
      },
      fileNames => {
        if (fileNames === undefined) {
          console.log('You didn\'t add any IRs')
          return
        }
        fileNames.forEach(fileName => {
          if (state.list.length < 128) {
            state.list.push({name: fileName.replace(/^.*[\\\/]/, '').slice(0, -4), fileName: fileName})
            state.isListDirty = true
          }
        })
      }
    )
  },

  setListDirty (state) {
    state.isListDirty = true
  }
}

const actions = {
  newBundle: ({ commit, state }) => {
    if (state.isListDirty) {
      remote.dialog.showMessageBox(
        {
          type: 'question',
          buttons: ['Yes', 'No'],
          message: 'Continue without saving?'
        },
        response => {
          if (response === 0) {
            commit('newBundle')
          }
        }
      )
    } else {
      commit('newBundle')
    }
  },

  openBundle: ({ commit, state }) => {
    if (state.isListDirty) {
      remote.dialog.showMessageBox(
        {
          type: 'question',
          buttons: ['Yes', 'No'],
          message: 'Continue without saving?'
        },
        response => {
          if (response === 0) {
            commit('openBundle')
          }
        }
      )
    } else {
      commit('openBundle')
    }
  },

  saveBundle: ({ commit, state }) => {
    if (state.isListDirty && state.fileName !== null) {
      commit('saveBundle')
    }
  },

  saveBundleAs: ({ commit }) => {
    commit('saveBundleAs')
  },

  exportBundle: ({ commit }) => {
    commit('exportBundle')
  },

  addIR: ({ commit, state }) => {
    if (state.list.length < 128) {
      commit('addIR')
    } else {
      remote.dialog.showErrorBox('Cannot add another IR', 'You have reached the maximum number of 128.')
    }
  },

  setListDirty: ({ commit }) => {
    commit('setListDirty')
  }
}

export default new Vuex.Store({
  state,
  mutations,
  actions
})

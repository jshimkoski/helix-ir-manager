import 'bootstrap-sass'
import Vue from 'vue'
import store from './store/store'
import Sortable from 'vue-sortable'
import App from './App'

Vue.use(Sortable)

/* eslint-disable no-new */
new Vue({
  store,
  el: 'body',
  components: { App }
})
